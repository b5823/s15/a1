/*CREATION START*/

let myName = 'Anthony';
let firstName = 'First Name:' + ' ' + myName;
console.log(firstName); 

let mySurname = 'Ferrer';
let lastName = 'Last Name:' + ' ' + mySurname;
console.log(lastName); 

let myAge = 23;
console.log("Age:" + ' ' + myAge);

let myHobbies = ["Dancing" , "Watching Netflix", "Reading Books/Mangas"];
console.log("Hobbies:")
console.log(myHobbies);

let myAddress = {
	houseNumber: "B11 L7",
	street: "Guyabano",
	city: "Santa Rosa",
	isActive: false
};
console.log("Work Address:");
console.log(myAddress);

/*CREATION END*/

/* DEBUGGING START*/

let name = "Steve Rogers";
console.log("My full name is:" + ' ' + name);

let currentAge = 40;
console.log("My current age is:" + ' ' + currentAge);

let friends = ["Tony" , "Bruce", "Thor" , "Natasha" , "Clint" , "Nick"];
console.log("My Friends are:")
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false

};
console.log("My Full Profile:");
console.log(profile);

let fullName = "Bucky Barnes";
console.log("My bestfriend is:" + fullName);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);

/* DEBUGGING END*/